/**
 * @Name: 基于layui
 * @Author: 潘晨晨
 * 最近修改时间: 2021/04/22
 */

layui.define(['jquery'],function(exports){
  var $ = layui.jquery;
  
    var obj ={
      init:function(element){
        //默认
        element.data = element.data || [ {name:'暂无数据'}]
        element.rows = element.rows || 1
        element.moreUpText = element.moreUpText || '更多'
        element.moreDownText = element.moreDownText || '收起'
        element.href = element.href || false
        element.herfBlank = element.herfBlank?'_blank':'_self'
        element.themeColor = element.themeColor || 'green'
        element.size = element.size ? 'layui-btn-'+element.size :'layui-btn'

        var html;
        var box = $(element.elemId);
        var boxHeight ;
        var itemHeight ;
        var flag = true; // 如果外面包裹了pack的变false
        var dyc = false;
        box.addClass('box-item-pcc');
        element.data.forEach(e=>{
          boxHeight = box.height();
          itemHeight = $('.item-pcc').outerHeight(true);
          element.href ? html = '<a class="item-pcc layui-bg-'+element.themeColor+' '+element.size+'" href="'+element.href+'?id='+e.id+'"  target="'+element.herfBlank+'">'+e.name+'</a>':html = '<span class="item-pcc layui-bg-'+element.themeColor+' '+element.size+'">'+e.name+'</span>'
            if((boxHeight>itemHeight*element.rows) && flag){
              morePosition();
              flag =false;
            }
            box.append(html);
        })
        
        
        boxHeight = box.height();
        itemHeight = $('.item-pcc').outerHeight(true);

        if(boxHeight>itemHeight*element.rows){
          more();   //第一次渲染页面
          dyc =true
        }
       
        
        $(document).on('click','#more-pcc',function(){ //点击更多
          more()
        })

        function morePosition(el){
          if(flag){
            if(el&&dyc){  //如果不是第一次渲染并且
              box.find('#more-box-pcc').remove();
            }
            else{
              $('.item-pcc').wrapAll("<span class='pack-pcc'></span>")
            }
            var list = $('.pack-pcc .item-pcc').eq(-3);
            list.before('<span id="more-box-pcc" class="item-pcc more-pcc '+element.size+'"><a class="layui-font-'+element.themeColor+' " id="more-pcc">'+element.moreUpText+'</a></span>');
           
          }
        }

        function more(){
          if(box.height()>itemHeight*element.rows){
            box.css('height',itemHeight*element.rows);
            morePosition(true)
          }else{
            box.css('height','auto');
            box.find('#more-box-pcc').remove();
            box.append('<span id="more-box-pcc" class="item-pcc more-pcc '+element.size+'"><a class="layui-font-'+element.themeColor+'" id="more-pcc">'+element.moreDownText+'</a></span>');
            flag = true;
          }
        
        }
        
      }
     
    }
    var $style = $('<style type="text/css">\
    .box-item-pcc { overflow: hidden; display: inline-block; width: 70%;} \
    .box-item-pcc .item-pcc {  margin:0 5px 5px 5px; box-sizing: border-box; display: inline-block; }\
    .box-item-pcc .more-pcc { float: right; background: none; }\
    .box-item-pcc a { cursor: pointer; }\
    .layui-font-red{color:#FF5722!important}\
    .layui-font-orange{color:#FFB800!important}\
    .layui-font-green{color:#009688!important}\
    .layui-font-cyan{color:#2F4056!important}\
    .layui-font-blue{color:#01AAED!important}\
    .layui-font-black{color:#000!important}\
    .layui-font-gray{color:#c2c2c2!important}\
      </style>');
    $($('head')[0]).append($style);
    
    exports('moreMenus',obj);
  });
  
